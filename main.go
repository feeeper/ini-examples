package main

import (
	"gopkg.in/ini.v1"
	"log"
)

type Config struct {
	ID string `ini:"id"`
	Index int `ini:"index"`
	GUID string `ini:"guid"`
	IsActive bool `ini:"isActive"`
	Balance string `ini:"balance"`
	Picture string `ini:"picture"`
	Age int `ini:"age"`
	EyeColor string `ini:"eyeColor"`
	Name struct {
		First string
		Last string
	} `ini:"name"`
	Company string `ini:"company"`
	Email string `ini:"email"`
	Phone string `ini:"phone"`
	Address string `ini:"address"`
	About string `ini:"about"`
	Registered string `ini:"registered"`
	Latitude string `ini:"latitude"`
	Longitude string `ini:"longitude"`
	Tags []string `ini:"tags"`
	Range []int `ini:"range"`
	Friends []string `ini:"friends"`
	Greeting string `ini:"greeting"`
	FavoriteFruit string `ini:"favoriteFruit"`
	SpecialField string `ini:"my_tagged_filed"`
}

type T struct {
	OutsideKey string
	AwesomeSection AwesomeSection `ini:"Awesome Section"`
}

type AwesomeSection struct {
	StringValue string
	IntValue int
}

type T2 struct {
	Parent Parent
}

type Parent struct {
	Message string
	Child Child `ini:"Parent.Child"`
}

type Child struct {
	Message string
}

func main() {
	data := `
	OutsideKey=Outside Value

	[Awesome Section]
	StringValue=Hello World!
	IntValue=42
	`
	cfg, err := ini.Load([]byte(data))
	if err != nil {
		log.Fatal(err)
	}

	section, _ := cfg.GetSection("")
	k, _ := section.GetKey("OutsideKey")
	log.Printf("OutsideKey: %s", k)

	section, _ = cfg.GetSection("Awesome Section")
	k, _ = section.GetKey("StringValue")
	log.Printf("Awesome Section.StringValue: %s", k)

	k, _ = section.GetKey("IntValue")
	log.Printf("Awesome Section.IntValue: %s", k)

	t := T{}
	err = cfg.MapTo(&t)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(t.OutsideKey)
	log.Println(t.AwesomeSection)

	data = `
	[Parent]
	Message = Parent Text

	[Parent.Child]
	Message = Child Text
	`
	cfg, err = ini.Load([]byte(data))
	if err != nil {
		log.Fatal(err)
	}
	t2 := T2{}
	err = cfg.MapTo(&t2)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(t2.Parent.Message)
	log.Println(t2.Parent.Child.Message)

	cfg, err = ini.Load("config.ini")
	if err != nil {
		log.Fatal(err)
	}

	config := Config{}
	cfg.MapTo(&config)
	log.Println(config.ID)
	log.Println(config.Friends)
	log.Println(config.Range)
	log.Println(config.SpecialField)
}
